let characters = [
    {id: 1, name: "Mario", stats: {str: 50, spd: 65, jmp: 60}, winRate: 40, cuteness: false,},
    {id: 2, name: "Pikachu", stats: {str: 40, spd: 90, jmp: 75}, winRate: 80, cuteness: true,},
    {id: 3, name: "DonkeyKong", stats: {str: 90, spd: 30, jmp: 40}, winRate: 65, cuteness: false,},
    {id: 4, name: "Samus", stats: {str: 70, spd: 35, jmp: 40}, winRate: 40, cuteness: true,},
    {id: 5, name: "Kirby", stats: {str: 55, spd: 85, jmp: 95}, winRate: 76, cuteness: true,},
    {id: 6, name: "Link", stats: {str: 65, spd: 60, jmp: 65}, winRate: 50, cuteness: false,},

]

/*Tenemos a 6 de los luchadores del SSB original. Filtramos a varios por su nivel de adorabilidad, porque nos gusta
golpear, pero con estilo y ojos brillantes*/
// let cuteChars = characters.filter(
//     function (pick) {
//         return pick.cuteness;
//     }
// );

// console.log(cuteChars);

/*Una vez tenemos a los tres más adorables, nos quedaremos  con los que tengan el mejor ratio de victorias,
seleccionado solo a los que estén por encima de 50*/

// let powerfulCh = cuteChars.filter (
//     function (cutePick) {
//         return cutePick.winRate > 50;
//     }
// )

// console.log(powerfulCh);

/*Con los dos finalistas, hacemos media de sus stats básicos y luego su ratio de victorias para obtener una puntuación
sobre 100 */

// let charStatus = powerfulCh.map (
//     function (champion) {
//         return (((champion.stats.jmp + champion.stats.spd +champion.stats.str)/3) + champion.winRate)/2;
//     }
// )

// console.log(charStatus);

/*Podríamos nuevamente filtrar para saber quién es el ganador (Kirby, obviamente), pero mi amigo el Rulas se ha
pasado por casa y trae su mando de la Switch para jugar en equipo online. Además es muy fan de Pikachu, así que
no habrá problemas a la hora de elegir personaje. Ahora calculamos la puntuación total de nuestro equipo sobre 200 */

// let teamPower = charStatus.reduce (
//     function (acc, statPoints) {
//         return (acc + statPoints)
//     }, 0
// )

// console.log(teamPower);

/*Nuestro equipo tiene una puntuación combinada de 151'3, una buena media que nos nos servírá para nada, porque
 Rulas y yo somos muy mancos y los jugadores japoneses muchos y acabaremos la tarde jugando a los tazos.*/

//  let teamPower = characters
//  .filter(
//     function (pick) {
//         return pick.cuteness;
//     }
// )
// .filter( function (cutePick) {
//     return cutePick.winRate > 50;
// })
// .map (
//     function (champion) {
//         return (((champion.stats.jmp + champion.stats.spd +champion.stats.str)/3) + champion.winRate)/2;
//     }
// )
// .reduce (
//     function (acc, statPoints) {
//         return (acc + statPoints)
//     }, 0
// );

// let teamPower = characters.filter(pick => pick.cuteness)
// .filter(cutePick => (cutePick.winRate > 50))
// .map (champion => (((champion.stats.jmp + champion.stats.spd +champion.stats.str)/3) + champion.winRate)/2)
// .reduce ((acc, statPoints) => acc + statPoints, 0);



let teamPower = characters.filter(pick => pick.cuteness && pick.winRate > 50)
.map (champion => (((champion.stats.jmp + champion.stats.spd +champion.stats.str)/3) + champion.winRate)/2)
.reduce ((acc, statPoints) => acc + statPoints, 0);

console.log(teamPower);